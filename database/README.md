# Golf MIS

## description
- this holds the database design for components of the system

## components
----

### **default**
- this will appear on all the tables unless specified

| defaults     |          | options |
| ------------ | -------- | ------- |
| id           | int      |         |
| date_created | datetime |         |
| date_updated | datetime |         |
| active       | bool     |         |


### **club member information management**
- this will also be used for guests

| users      |        | options                        |
| ---------- | ------ | ------------------------------ |
| type       | string | member, guest, admin, employee |
| status     | string |                                |
| first_name | string |                                |
| middle_name | string |                                |
| last_name  | string |                                |
| gender     | string |                                |
| birthdate  | date   |                                |


### **tee time reservation**
- reservations are done in intervals of 15 mins

| tee_time_reservation |          | options                             |
| -------------------- | -------- | ----------------------------------- |
| time                 | datetime | sql format datetime                 |
| user_id              | int      | this could be from a user or system |
| golf_course_id       | int      |                                     |
| type                 | string   | "user",  "event"                    |
| status               | string   |                                     |
| name                 | text     |                                     |
| details          | text     |                                     |


| tee_time_reservation_members |        | options                             |
| ---------------------------- | ------ | ----------------------------------- |
| tee_time_reservation_id      | int    |                                     |
| user_id                      | int    | this could be from a user or system |
| type                         | string | "member", "guest"                   |
| details                  | text   |                                     |
