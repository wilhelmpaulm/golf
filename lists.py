# import logging
# import os
import datetime
# import os.path
# import json

# from flask import Flask, render_template, request, flash, redirect, url_for
from flask import Flask
# from flask_sqlalchemy import SQLAlchemy
from flask_mail import Mail
# , Message
# from flask_security import Security, SQLAlchemyUserDatastore
# from flask_security import login_required, current_user
from flask_security import current_user

from utils import wtf

from config import BaseConfig
from helpers import MethodRewriteMiddleware

from models import db
# from models import Role
from models import User
from models import GolfCourse
# from models import TeeTimeReservation, TeeTimeReservationMembers


# Create app
app = Flask(BaseConfig.PROJECT_NAME)
app.config.from_object(BaseConfig)
app.wsgi_app = MethodRewriteMiddleware(app.wsgi_app)
app.jinja_env.filters['alert_class'] = False  # alert_class_filter

# Create Mail obj
mail = Mail(app)

# WTForms helpers
wtf.add_helpers(app)


# Import helpers
# Create database connection object
db.init_app(app)


def get_user_roles():
    return ['admin',  'user', 'guest', 'employee']


def get_user_types():
    return get_user_roles()


def time_slots(start_time, end_time):
    time_slots = [start_time]
    date_today = datetime.datetime.today().date()

    while time_slots[-1] < end_time:
        datetime_base = datetime.datetime.combine(
            date_today, time_slots[-1])
        datetime_base += datetime.timedelta(minutes=15)
        time_slots.append(datetime_base.time())

    return time_slots


def get_tee_times():
    start_time = datetime.time(8, 00)
    end_time = datetime.time(17, 00)
    return time_slots(start_time, end_time)


def get_str_tee_times():
    start_time = datetime.time(8, 00)
    end_time = datetime.time(17, 00)
    return [str(slot)[:5] for slot in time_slots(start_time, end_time)]


def get_tee_times_dict():
    start_time = datetime.time(8, 00)
    end_time = datetime.time(17, 00)
    return {str(slot)[:5]: slot.strftime("%I:%M %p") for slot in time_slots(start_time, end_time)}


def get_golf_courses_dict():
    """ add rules here when getting the golf courses """
    golf_courses = []
    with app.app_context():
        golf_courses = GolfCourse.query.all()
    return {golf_course.id: golf_course.name for golf_course in golf_courses}


def members_dict():
    """ add rules here when getting the tee time members """
    users = []
    with app.app_context():
        users = User.query.all()
        # [(i) for i in members_dict() if i=="two"]
        return {user.id: "%s, %s (%s)" % (user.last_name, user.first_name, user.email) for user in users if user.id != current_user.id}
    return []

def get_item_types():
    return ['sale', 'service', 'rental']
