import datetime
# import logging
import os
import json
import random
# import os.path
# import json

from flask import Flask, render_template, request, flash, redirect, url_for
# from flask_sqlalchemy import SQLAlchemy
from flask_mail import Mail
# , Message
from flask_security import Security, SQLAlchemyUserDatastore
from flask_security import login_required, current_user

from utils import wtf

from config import BaseConfig
from helpers import MethodRewriteMiddleware
from helpers import combine_date_time_str, get_date_from_str
from helpers import get_random

from models import db
from models import User, Role
from models import GolfCourse, TeeTimeReservation, TeeTimeReservationMember
from models import Facility, FacilityReservation
from models import InventoryItem, Payment
from models import InvoiceItem, Invoice
# from models import TeeTimeReservation, TeeTimeReservationMembers

from lists import get_golf_courses_dict, get_tee_times_dict, members_dict
from lists import get_item_types

# Create app
app = Flask(BaseConfig.PROJECT_NAME)
app.config.from_object(BaseConfig)
app.wsgi_app = MethodRewriteMiddleware(app.wsgi_app)
app.jinja_env.filters['alert_class'] = False  # alert_class_filter

# Create Mail obj
mail = Mail(app)

# WTForms helpers
wtf.add_helpers(app)

# Import helpers
# Create database connection object
db.init_app(app)

# Setup Flask-Security
user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security = Security(app, user_datastore)

# Setup Faker for creating fake DB entries
from faker import Faker
fake = Faker()
fake.seed(1234)  # this ensures the same data is produced


def current_user_roles():
    return [role.name for role in current_user.roles]


@app.route('/migrate')
@app.route('/migrate_db')
@app.route('/db_migrate')
@app.route('/init_db')
@app.route('/db_init')
@app.route('/db')
def migrate_db():
    try:
        os.remove('database/golf.db')
    except OSError:
        print("failed delete")

    db.create_all(app=app)

    user_datastore.create_role(name='admin')
    user_datastore.create_role(name='user')

    user_datastore.create_user(
        username='admin',
        email='admin@example.com',
        first_name='admin man',
        last_name='user',
        password='password', roles=['admin'])

    user_datastore.create_user(
        username='user',
        email='user@example.com',
        first_name='sample man',
        last_name='user',
        password='password', roles=['user'])

    db.session.commit()

    for num in range(0, 50):
        first_name = fake.first_name()
        last_name = fake.last_name()
        email = fake.email()
        user_datastore.create_user(
            username="num%s" % (first_name),
            email=email,
            first_name=first_name,
            last_name=last_name,
            birthdate=fake.date_between(start_date="-30y", end_date="today"),
            home_address=fake.address(),
            billing_address=fake.address(),
            mobile_number="+63%s" % (random.randint(9100000000, 9999999999)),
            landline_number="02%s" % (random.randint(5000000, 9999999)),
            gender=get_random(['male', 'female']),
            type=get_random(['member', 'guest', 'employee']),
            password='password', roles=['user'])

    for num in range(0, 8):
        course_name = "%s Course" % (fake.first_name())
        golf_course = GolfCourse(
            name=course_name,
            details=fake.text(),
            price=round(random.uniform(300.00, 500.00), 2),
            active=True,
            type=get_random(['18 holes', '9 holes', '36 holes']))
        db.session.add(golf_course)

    for num in range(0, 5):
        name = "Golf Cart %s" % (num)
        item = InventoryItem(
            name=name,
            details=fake.text(),
            quantity=1,
            price=500,
            type='rental',
            active=True)
        db.session.add(item)

    for num in range(0, 8):
        name = "%s Facility" % (fake.first_name())
        facility = Facility(
            name=name,
            details=fake.text(),
            active=True,
            capacity=random.randint(10, 50),
            price=round(random.uniform(3000.5, 15000.5), 2),
            type=get_random(['18 holes', '9 holes', '36 holes']))
        db.session.add(facility)

    db.session.commit()

    return "migrate successful"


@app.before_first_request
def create_users():
    """
        Create a user to test with
    """
    db.create_all(app=app)


"""
    flask security routes
"""


@app.route('/after-login')
@login_required
def afterLogin():
    if current_user.has_role('admin'):
        return redirect('admin')
    elif current_user.has_role('user'):
        return 'you are a regular user'
    else:
        logout()
        return redirect('/')

    # return json.dumps(dir(current_user))


"""
    start of views
"""


@app.route('/')
# @login_required
def index():
    return redirect('login')
    # return render_template('index.html')


@app.route('/admin')
@login_required
def admin_index():
    return render_template('admin/index.html')


@app.route('/admin/forms')
@login_required
def admin_forms():
    return render_template('admin/forms.html')


"""
    invoice routes
"""


@app.route('/invoices')
def list_invoices():
    """
        GET /invoices 
        Lists all invoices
    """
    invoices = Invoice.query.all()  # Your query here ;)
    return render_template('invoice/list.html', invoices=invoices)


@app.route('/invoices/<id>')
def show_invoice(id):
    """
        GET /invoices/<id>
        Get a invoice by its id
    """
    invoice = Invoice.query.get(id)
    invoice.user = invoice.get_user()
    return render_template('invoice/show.html', invoice=invoice)


@app.route('/invoices/<id>/payments')
def list_invoice_reservations_by_invoice_id(id):
    """
        GET /invoices/<id>/reservations
        Get reservations of a invoice
    """
    payments = Payment.query.filter_by(
        invoice_id=id)  # Your query here ;)
    return render_template(
        'payments/list.html',
        payments=payments)


@app.route('/invoices/new')
def new_invoice():
    """
        GET /invoices/new 
        The form for a new invoice
    """
    return render_template('invoice/new.html')


@app.route('/invoices', methods=['POST'])
def create_invoice():
    """
        POST /invoices 
        Receives a invoice data and saves it
    """
    # type = request.form.get('type')
    name = request.form.get('name')
    details = request.form.get('details')
    price = request.form.get('price')
    active = bool(request.form.get('active') == 'true')
    invoice = Invoice(name=name, details=details,
                      price=price, active=active)  # Save it
    db.session.add(invoice)
    db.session.commit()
    db.session.flush()

    flash('Invoice %s successfully saved!' % invoice.name)
    return redirect(url_for('show_invoice', id=invoice.id))


@app.route('/invoices/<id>/edit')
def edit_invoice(id):
    """
        GET /invoices/<id>/edit
        Form for editing a invoice
    """
    invoice = Invoice.query.get(id)
    return render_template('invoice/edit.html', invoice=invoice)


@app.route('/invoices/<id>', methods=['PUT'])
def update_invoice(id):
    """
        PUT /invoices/<id>
        Updates a invoice
    """
    invoice = Invoice.query.get(id)
    invoice.name = request.form.get('name')  # Save it
    invoice.details = request.form.get('details')  # Save it
    invoice.price = request.form.get('price')  # Save it
    invoice.active = bool(request.form.get('active')) == 'true'  # Save it
    flash('Invoice %s updated!' % invoice.name)
    db.session.add(invoice)
    db.session.commit()
    return redirect(url_for('show_invoice', id=invoice.id))


@app.route('/invoices/<id>', methods=['DELETE'])
def delete_invoice(id):
    """
        DELETE /invoices/<id>
        Deletes a invoices
    """
    invoice = Invoice.query.get(id)
    db.session.delete(invoice)
    db.session.commit()
    flash('Invoice %s deleted!' % invoice.name)
    return redirect(url_for('list_invoices'))


"""
    facility routes
"""


@app.route('/facilities')
def list_facilities():
    """
        GET /facilities 
        Lists all facilities
    """
    facilities = Facility.query.all()  # Your query here ;)
    return render_template(
        'facility/list.html',
        facilities=facilities)


@app.route('/facilities/<id>')
def show_facility(id):
    """
        GET /facilities/<id>
        Get a facility by its id
    """
    facility = Facility.query.get(id)
    return render_template('facility/show.html', facility=facility)


@app.route('/facilities/<id>/reservations')
def list_facility_reservations_by_facility_id(id):
    """
        GET /facilities/<id>/reservations
        Get reservations of a facility
    """
    facility_reservations = FacilityReservation.query.filter_by(
        facility_id=id)  # Your query here ;)
    return render_template(
        'facility_reservation/list.html',
        facility_reservations=facility_reservations)


@app.route('/facilities/new')
def new_facility():
    """
        GET /facilities/new 
        The form for a new facility
    """
    return render_template('facility/new.html')


@app.route('/facilities', methods=['POST'])
def create_facility():
    """
        POST /facilities 
        Receives a facility data and saves it
    """
    # type = request.form.get('type')
    name = request.form.get('name')
    details = request.form.get('details')
    price = request.form.get('price')
    active = bool(request.form.get('active') == 'true')
    facility = Facility(name=name, details=details,
                        price=price, active=active)  # Save it
    db.session.add(facility)
    db.session.commit()
    db.session.flush()

    flash('Facility %s successfully saved!' % facility.name)
    return redirect(url_for('show_facility', id=facility.id))


@app.route('/facilities/<id>/edit')
def edit_facility(id):
    """
        GET /facilities/<id>/edit
        Form for editing a facility
    """
    facility = Facility.query.get(id)
    return render_template('facility/edit.html', facility=facility)


@app.route('/facilities/<id>', methods=['PUT'])
def update_facility(id):
    """
        PUT /facilities/<id>
        Updates a facility
    """
    facility = Facility.query.get(id)
    facility.name = request.form.get('name')  # Save it
    facility.details = request.form.get('details')  # Save it
    facility.price = request.form.get('price')  # Save it
    facility.active = bool(request.form.get('active')) == 'true'  # Save it
    flash('Facility %s updated!' % facility.name)
    db.session.add(facility)
    db.session.commit()
    return redirect(url_for('show_facility', id=facility.id))


@app.route('/facilities/<id>', methods=['DELETE'])
def delete_facility(id):
    """
        DELETE /facilities/<id>
        Deletes a facilities
    """
    facility = Facility.query.get(id)
    db.session.delete(facility)
    db.session.commit()
    flash('Facility %s deleted!' % facility.name)
    return redirect(url_for('list_facilities'))


"""
    facility reservation routes
"""


@app.route('/facility_reservations')
def list_facility_reservations():
    """
        GET /facility_reservations 
        Lists all facility_reservations
    """
    facility_reservations = FacilityReservation.query.all()  # Your query here ;)
    return render_template(
        'facility_reservation/list.html',
        facility_reservations=facility_reservations)


@app.route('/facility_reservations/<id>')
def show_facility_reservation(id):
    """
        GET /facility_reservations/<id> 
        Get a facility by its id
    """
    facility_reservation = FacilityReservation.query.get(id)
    return render_template(
        'facility_reservation/show.html',
        facility_reservation=facility_reservation)


@app.route('/facility_reservations/new')
def new_facility_reservation():
    """
        GET /facility_reservations/new
        The form for a new facility
    """
    vars = {
        'facilities': Facility.query.all(),
        'datetime': datetime
    }
    return render_template('facility_reservation/new.html', vars=vars)


@app.route('/facility_reservations', methods=['POST'])
def create_facility_reservation():
    """
        POST /facility_reservations 
        Receives a facility data and saves it
    """
    user_id = current_user.id
    facility_id = request.form.get('facility_id')
    name = request.form.get('name')
    details = request.form.get('details')
    date_start = get_date_from_str(request.form.get('date_start'))
    date_end = get_date_from_str(request.form.get('date_end'))
    status = 'pending'
    reservation = FacilityReservation(user_id=user_id, name=name, details=details, facility_id=facility_id,
                                      status=status, date_start=date_start, date_end=date_end)  # Save it
    db.session.add(reservation)
    db.session.commit()
    db.session.flush()

    flash('Reservation for  %s successfully saved!' % reservation.name)
    return redirect(url_for('show_facility_reservation', id=reservation.id))


@app.route('/facility_reservations/<id>/edit')
def edit_facility_reservation(id):
    """
        GET /facility_reservations/<id>/edit
        Form for editing a facility
    """
    reservation = FacilityReservation.query.get(id)
    vars = {
        'facilities': Facility.query.all(),
        'datetime': datetime
    }
    return render_template('facility_reservation/edit.html', facility_reservation=reservation, vars=vars)


@app.route('/facility_reservations/<id>', methods=['PUT'])
def update_facility_reservation(id):
    """
        PUT /facility_reservations/<id>
        Updates a facility
    """
    reservation = FacilityReservation.query.get(id)
    reservation.name = request.form.get('name')  # Save it
    reservation.details = request.form.get('details')  # Save it
    reservation.date_start = get_date_from_str(request.form.get('date_start'))
    reservation.date_end = get_date_from_str(request.form.get('date_end'))
    if (current_user.has_role('admin')):
        reservation.status = request.form.get('status')  # Save it

    if (reservation.status == 'approved'):
        invoice = Invoice(user_id=reservation.user_id,
                          details='invoice prepared for facility reservation')
        item_details = "name: %s,\n date_start: %s,\n date_end: %s" % (
            reservation.name, reservation.date_start, reservation.date_end)
        item_price = reservation.facility.price * reservation.get_days()
        item = InvoiceItem(
            type='service',
            name='facility reservation',
            details=item_details,
            price=item_price)
        invoice.invoice_items.append(item)
        db.session.add(invoice)

    flash('Reservation %s updated!' % reservation.name)
    db.session.add(reservation)
    db.session.commit()
    return redirect(url_for('show_facility_reservation', id=reservation.id))


@app.route('/facility_reservations/<id>/approve', methods=['PUT'])
def approve_facility_reservation(id):
    """
        PUT /facility_reservations/<id>/approve
        Approve the reservation
    """
    reservation = FacilityReservation.query.get(id)
    reservation.status = 'approved'
    db.session.add(reservation)

    invoice = Invoice(user_id=reservation.user_id,
                      details='invoice prepared for facility reservation')
    item_details = "name: %s,\n date_start: %s,\n date_end: %s" % (
        reservation.name, reservation.date_start, reservation.date_end)
    item_price = reservation.facility.price * reservation.get_days()
    item = InvoiceItem(
        type='service',
        name='facility reservation',
        details=item_details,
        price=item_price)
    invoice.invoice_items.append(item)
    db.session.add(invoice)

    flash('Reservation %s updated!' % reservation.name)
    db.session.commit()
    return redirect(url_for('show_facility_reservation', id=reservation.id))


@app.route('/facility_reservations/<id>', methods=['DELETE'])
def delete_facility_reservation(id):
    """
        DELETE /facility_reservations/<id>
        Deletes a facility_reservations
    """
    reservation = FacilityReservation.query.get(id)
    db.session.delete(reservation)
    db.session.commit()
    flash('FacilityReservation %s deleted!' % reservation.name)
    return redirect(url_for('list_facility_reservations'))


"""
    golf course routes
"""


@app.route('/golf_courses')
def list_golf_courses():
    """GET /golf_courses
    Lists all golf_courses"""
    golf_courses = GolfCourse.query.all()  # Your query here ;)
    return render_template(
        'golf_course/list_golf_courses.html',
        golf_courses=golf_courses)


@app.route('/golf_courses/<id>')
def show_golf_course(id):
    """GET /golf_courses/<id>
    Get a golf_course by its id"""
    golf_course = GolfCourse.query.get(id)
    return render_template(
        'golf_course/show_golf_course.html',
        golf_course=golf_course)


@app.route('/golf_courses/new')
def new_golf_course():
    """GET /golf_courses/new
    The form for a new golf_course"""
    return render_template('golf_course/new_golf_course.html')


@app.route('/golf_courses', methods=['POST'])
def create_golf_course():
    """POST /golf_courses
    Receives a golf_course data and saves it"""
    name = request.form.get('name')
    type = request.form.get('type')
    details = request.form.get('details')
    golf_course = GolfCourse(name=name, type=type, details=details)  # Save it
    db.session.add(golf_course)
    db.session.commit()
    db.session.flush()

    flash('Golf Course %s successfully saved!' % golf_course.name)
    return redirect(url_for('show_golf_course', id=golf_course.id))


@app.route('/golf_courses/<id>/edit')
def edit_golf_course(id):
    """GET /golf_courses/<id>/edit
    Form for editing a golf_course"""
    golf_course = GolfCourse.query.get(id)
    return render_template('golf_course/edit_golf_course.html', golf_course=golf_course)


@app.route('/golf_courses/<id>', methods=['PUT'])
def update_golf_course(id):
    """PUT /golf_courses/<id>
    Updates a golf_course"""
    golf_course = GolfCourse.query.get(id)
    golf_course.name = request.form.get('name')  # Save it
    golf_course.type = request.form.get('type')  # Save it
    golf_course.details = request.form.get('details')  # Save it
    flash('Golf Course %s updated!' % golf_course.name)
    db.session.add(golf_course)
    db.session.commit()
    return redirect(url_for('show_golf_course', id=golf_course.id))


@app.route('/golf_courses/<id>', methods=['DELETE'])
def delete_golf_course(id):
    """DELETE /golf_courses/<id>
    Deletes a golf_courses"""
    golf_course = GolfCourse.query.get(id)
    db.session.delete(golf_course)
    db.session.commit()
    flash('Golf Course %s deleted!' % golf_course.name)
    return redirect(url_for('list_golf_courses'))


"""
    payment routes
"""


@app.route('/users')
def list_users():
    """
        GET /users
        Lists all users
    """
    users = User.query.all()  # Your query here ;)
    return render_template('user/list.html', users=users)


@app.route('/users/<id>')
def show_user(id):
    """
        GET /users/<id>
        Get a user by its id
    """
    user = User.query.get(id)
    return render_template('user/show.html', user=user)


@app.route('/users/new')
def new_user():
    """
        GET /users/new
        The form for a new user
    """
    return render_template('user/new.html')


@app.route('/users', methods=['POST'])
def create_user():
    """
        POST /users
        Receives a user data and saves it
    """
    first_name = request.form.get('first_name')
    last_name = request.form.get('last_name')
    middle_name = request.form.get('middle_name')
    email = request.form.get('email')
    mobile_number = request.form.get('mobile_number')
    landline_number = request.form.get('landline_number')
    home_address = request.form.get('home_address')
    billing_address = request.form.get('billing_address')
    emergency_contact = request.form.get('emergency_contact')
    password = request.form.get('password')
    gender = request.form.get('gender')
    birthdate = get_date_from_str(request.form.get('birthdate'))
    company_name = request.form.get('company_name')
    company_address = request.form.get('company_address')
    company_position = request.form.get('company_position')
    password = request.form.get('password')
    type = request.form.get('type')
    user = User(
        first_name=first_name, last_name=last_name, middle_name=middle_name,
        email=email, mobile_number=mobile_number, landline_number=landline_number,
        birthdate=birthdate, gender=gender, password=password,
        home_address=home_address, billing_address=billing_address, company_address=company_address,
        type=type, company_position=company_position, company_name=company_name,
        emergency_contact=emergency_contact
    )
    db.session.add(user)
    db.session.commit()
    db.session.flush()

    flash('User %s successfully saved!' % user.id)
    return redirect(url_for('show_user', id=user.id))


@app.route('/users/<id>/edit')
def edit_user(id):
    """
        GET /users/<id>/edit
        Form for editing a user
    """
    user = User.query.get(id)
    return render_template('user/edit.html', user=user)


@app.route('/users/<id>', methods=['PUT'])
def update_user(id):
    """
        PUT /users/<id>
        Updates a user
    """
    user = User.query.get(id)
    user.first_name = request.form.get('first_name')
    user.last_name = request.form.get('last_name')
    user.middle_name = request.form.get('middle_name')
    user.email = request.form.get('email')
    user.mobile_number = request.form.get('mobile_number')
    user.landline_number = request.form.get('landline_number')
    user.home_address = request.form.get('home_address')
    user.billing_address = request.form.get('billing_address')
    user.emergency_contact = request.form.get('emergency_contact')
    user.password = request.form.get('password')
    user.gender = request.form.get('gender')
    user.birthdate = get_date_from_str(request.form.get('birthdate'))
    user.company_name = request.form.get('company_name')
    user.company_address = request.form.get('company_address')
    user.company_position = request.form.get('company_position')
    user.password = request.form.get('password')
    user.type = request.form.get('type')
    flash('User %s updated!' % user.id)
    db.session.add(user)
    db.session.commit()
    return redirect(url_for('show_user', id=user.id))


@app.route('/users/<id>', methods=['DELETE'])
def delete_user(id):
    """
        DELETE /users/<id>
        Deletes a users
    """
    user = User.query.get(id)
    db.session.delete(user)
    db.session.commit()
    flash('User %s deleted!' % user.id)
    return redirect(url_for('list_users'))


"""
    payment routes
"""


@app.route('/payments')
def list_payments():
    """
        GET /payments
        Lists all payments
    """
    payments = Payment.query.all()  # Your query here ;)
    return render_template('payment/list.html', payments=payments)


@app.route('/payments/<id>')
def show_payment(id):
    """
        GET /payments/<id>
        Get a payment by its id
    """
    payment = Payment.query.get(id)
    return render_template('payment/show.html', payment=payment)


@app.route('/payments/new')
def new_payment():
    """
        GET /payments/new
        The form for a new payment
    """
    vars = {
        'invoices': Invoice.query.all(),
    }
    return render_template('payment/new.html', vars=vars)


@app.route('/payments', methods=['POST'])
def create_payment():
    """
        POST /payments
        Receives a payment data and saves it
    """
    invoice_id = request.form.get('invoice_id')
    type = request.form.get('type', 'cash')
    amount = request.form.get('amount', 0.0)
    details = request.form.get('details', '')
    payment = Payment(invoice_id=invoice_id, type=type, amount=amount, details=details, user_id=current_user.id)  # Save it
    db.session.add(payment)
    db.session.commit()
    db.session.flush()

    flash('Payment %s successfully saved!' % payment.id)
    return redirect(url_for('show_payment', id=payment.id))


@app.route('/payments/<id>/edit')
def edit_payment(id):
    """
        GET /payments/<id>/edit
        Form for editing a payment
    """
    payment = Payment.query.get(id)
    vars = {
        'invoices': Invoice.query.all(),
    }
    return render_template('payment/edit.html', payment=payment, vars=vars)


@app.route('/payments/<id>', methods=['PUT'])
def update_payment(id):
    """
        PUT /payments/<id>
        Updates a payment
    """
    payment = Payment.query.get(id)
    payment.invoice_id = request.form.get('invoice_id')  # Save it
    payment.type = request.form.get('type')  # Save it
    payment.amount = request.form.get('amount')  # Save it
    payment.details = request.form.get('details')  # Save it
    flash('Payment %s updated!' % payment.id)
    db.session.add(payment)
    db.session.commit()
    return redirect(url_for('show_payment', id=payment.id))


@app.route('/payments/<id>', methods=['DELETE'])
def delete_payment(id):
    """
        DELETE /payments/<id>
        Deletes a payments
    """
    payment = Payment.query.get(id)
    db.session.delete(payment)
    db.session.commit()
    flash('Payment %s deleted!' % payment.id)
    return redirect(url_for('list_payments'))


"""
    inventory item routes
"""


@app.route('/inventory_items')
def list_inventory_items():
    """GET /inventory_items
    Lists all inventory_items"""
    inventory_items = InventoryItem.query.all()  # Your query here ;)
    return render_template(
        'inventory_item/list_inventory_items.html',
        inventory_items=inventory_items)


@app.route('/inventory_items/<id>')
def show_inventory_item(id):
    """GET /inventory_items/<id>
    Get a inventory_item by its id"""
    inventory_item = InventoryItem.query.get(id)
    return render_template(
        'inventory_item/show_inventory_item.html',
        inventory_item=inventory_item)


@app.route('/inventory_items/new')
def new_inventory_item():
    """GET /inventory_items/new
    The form for a new inventory_item"""
    vars = {
        'item_types': get_item_types(),
    }
    return render_template('inventory_item/new_inventory_item.html', vars=vars)


@app.route('/inventory_items', methods=['POST'])
def create_inventory_item():
    """POST /inventory_items
    Receives a inventory_item data and saves it"""
    name = request.form.get('name')
    type = request.form.get('type')
    details = request.form.get('details')
    quantity = request.form.get('quantity')
    price = request.form.get('price')
    inventory_item = InventoryItem(
        name=name, type=type, details=details, quantity=quantity, price=price)  # Save it
    db.session.add(inventory_item)
    db.session.commit()
    db.session.flush()

    flash('Item %s successfully saved!' % inventory_item.name)
    return redirect(url_for('show_inventory_item', id=inventory_item.id))


@app.route('/inventory_items/<id>/edit')
def edit_inventory_item(id):
    """GET /inventory_items/<id>/edit
    Form for editing a inventory_item"""
    vars = {
        'item_types': get_item_types(),
    }
    inventory_item = InventoryItem.query.get(id)
    return render_template('inventory_item/edit_inventory_item.html', inventory_item=inventory_item, vars=vars)


@app.route('/inventory_items/<id>', methods=['PUT'])
def update_inventory_item(id):
    """PUT /inventory_items/<id>
    Updates a inventory_item"""
    inventory_item = InventoryItem.query.get(id)
    inventory_item.name = request.form.get('name')  # Save it
    inventory_item.type = request.form.get('type')  # Save it
    inventory_item.details = request.form.get('details')  # Save it
    inventory_item.quantity = request.form.get('quantity')  # Save it
    inventory_item.price = request.form.get('price')  # Save it
    flash('Item %s updated!' % inventory_item.name)
    db.session.add(inventory_item)
    db.session.commit()
    return redirect(url_for('show_inventory_item', id=inventory_item.id))


@app.route('/inventory_items/<id>', methods=['DELETE'])
def delete_inventory_item(id):
    """DELETE /inventory_items/<id>
    Deletes a inventory_items"""
    inventory_item = InventoryItem.query.get(id)
    db.session.delete(inventory_item)
    db.session.commit()
    flash('Item %s deleted!' % inventory_item.name)
    return redirect(url_for('list_inventory_items'))


"""
    tee time reservation routes
"""


@app.route('/tee_time_reservations')
def list_tee_time_reservations():
    """GET /tee_time_reservations
    Lists all tee_time_reservations"""
    tee_time_reservations = TeeTimeReservation.query.all()  # Your query here ;)
    return render_template(
        'tee_time_reservation/list_tee_time_reservations.html',
        tee_time_reservations=tee_time_reservations)


@app.route('/tee_time_reservations/<id>')
def show_tee_time_reservation(id):
    """GET /tee_time_reservations/<id>
    Get a tee_time_reservation by its id"""
    tee_time_reservation = TeeTimeReservation.query.get(id)
    return render_template(
        'tee_time_reservation/show_tee_time_reservation.html',
        tee_time_reservation=tee_time_reservation)


@app.route('/tee_time_reservations/<id>/approve', methods=['PUT'])
def approve_tee_time_reservation(id):
    """
        PUT /invoices/<id>
        Approve a reservation
    """
    reservation = TeeTimeReservation.query.get(id)
    reservation.status = 'approved'  # Save it
    db.session.add(reservation)
    invoice = Invoice(user_id=reservation.user_id,
                      details='invoice prepared for tee time reservation')
    item_details = "course: %s,\n date & time: %s" % (
        reservation.golf_course.name, reservation.datetime)
    item_price = reservation.golf_course.price
    item = InvoiceItem(
        type='service',
        name='tee time reservation',
        details=item_details,
        price=item_price)
    invoice.invoice_items.append(item)
    db.session.add(invoice)

    flash('Tee Time Reservation  %s approved!' % id)
    db.session.commit()
    return redirect(url_for('list_tee_time_reservations'))


@app.route('/tee_time_reservations/new')
def new_tee_time_reservation():
    """GET /tee_time_reservations/new
    The form for a new tee_time_reservation"""
    vars = {
        'golf_courses': get_golf_courses_dict(),
        'tee_times': get_tee_times_dict(),
        'members': members_dict(),
        'datetime': datetime,
    }

    return render_template('tee_time_reservation/new_tee_time_reservation.html', vars=vars)


@app.route('/tee_time_reservations', methods=['POST'])
def create_tee_time_reservation():
    """POST /tee_time_reservations
    Receives a tee_time_reservation data and saves it"""
    print()
    reservation_date = request.form.get('date')
    reservation_time = request.form.get('time')
    member_ids = request.form.getlist('user_ids')
    reservation_datetime = combine_date_time_str(
        reservation_date, reservation_time)
    golf_course_id = request.form.get('golf_course_id')
    details = request.form.get('details')
    tee_time_reservation = TeeTimeReservation(
        user_id=current_user.id,
        datetime=reservation_datetime,
        golf_course_id=golf_course_id,
        members=json.dumps(member_ids),
        details=details)  # Save it
    db.session.add(tee_time_reservation)
    db.session.commit()
    db.session.flush()

    flash('Tee Time Reservation %s successfully saved!' %
          tee_time_reservation.name)
    return redirect(url_for('show_tee_time_reservation', id=tee_time_reservation.id))


# @app.route('/tee_time_reservations/<id>/edit')
# def edit_tee_time_reservation(id):
#     """GET /tee_time_reservations/<id>/edit
#     Form for editing a tee_time_reservation"""
#     tee_time_reservation = TeeTimeReservation.query.get(id)
#     return render_template('tee_time_reservation/edit_tee_time_reservation.html', tee_time_reservation=tee_time_reservation)


# @app.route('/tee_time_reservations/<id>', methods=['PUT'])
# def update_tee_time_reservation(id):
#     """PUT /tee_time_reservations/<id>
#     Updates a tee_time_reservation"""
#     tee_time_reservation = TeeTimeReservation.query.get(id)
#     tee_time_reservation.name = request.form.get('name')  # Save it
#     tee_time_reservation.type = request.form.get('type')  # Save it
#     tee_time_reservation.details = request.form.get('details')  # Save it
#     flash('Golf Course %s updated!' % tee_time_reservation.name)
#     db.session.add(tee_time_reservation)
#     db.session.commit()
#     return redirect(url_for('show_tee_time_reservation', id=tee_time_reservation.id))


@app.route('/tee_time_reservations/<id>', methods=['DELETE'])
def delete_tee_time_reservation(id):
    """DELETE /tee_time_reservations/<id>
    Deletes a tee_time_reservations"""
    tee_time_reservation = TeeTimeReservation.query.get(id)
    db.session.delete(tee_time_reservation)
    db.session.commit()
    flash('Tee Time Reservation %s deleted!' % tee_time_reservation.name)
    return redirect(url_for('list_tee_time_reservations'))


"""
    end of routes
"""


if __name__ == '__main__':
    app.run()
