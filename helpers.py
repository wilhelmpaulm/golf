from werkzeug.urls import url_decode
from flask_security.forms import LoginForm
import datetime
import random


class MethodRewriteMiddleware(object):
    """Middleware for HTTP method rewriting.
    Snippet: http://flask.pocoo.org/snippets/38/
    """

    def __init__(self, app):
        self.app = app

    def __call__(self, environ, start_response):
        if 'METHOD_OVERRIDE' in environ.get('QUERY_STRING', ''):
            args = url_decode(environ['QUERY_STRING'])
            method = args.get('__METHOD_OVERRIDE__')
            if method:
                method = method.encode('ascii', 'replace')
                environ['REQUEST_METHOD'] = method
        return self.app(environ, start_response)


class CustomLoginForm(LoginForm):
    def validate(self):
        # Put code here if you want to do stuff before login attempt

        response = super(CustomLoginForm, self).validate()

        # Put code here if you want to do stuff after login attempt

        return response

# Bootstrap helpers


def alert_class_filter(category):
    # Map different message types to Bootstrap alert classes
    categories = {
        "message": "warning"
    }
    return categories.get(category, category)


def get_date_from_str(str_date):
    return datetime.datetime.strptime(str_date, "%Y-%m-%d")


def get_time_from_str(str_time):
    return datetime.datetime.strptime(str_time, "%H:%M").time()


def get_datetime_from_str(str_time):
    return datetime.datetime.strptime(str_time, "%Y-%m-%d %H:%M:%S")


def combine_date_time_str(str_date, str_time):
    return datetime.datetime.combine(
        get_date_from_str(str_date), get_time_from_str(str_time))


def combine_date_time(d, s):
    return datetime.datetime.combine(d, s)


def get_random(list):
    if not list:
        return None
    return list[random.randint(0, len(list) - 1)]
