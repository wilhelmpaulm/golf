from flask_sqlalchemy import SQLAlchemy
from flask_security import UserMixin, RoleMixin
import datetime
import json

# Create app
db = SQLAlchemy()

# Define models


class RolesUsers(db.Model):
    __tablename__ = 'roles_users'
    created_at = db.Column(db.DateTime(),  default=datetime.datetime.utcnow)
    updated_at = db.Column(db.DateTime(),  onupdate=datetime.datetime.utcnow)

    id = db.Column(db.Integer(), primary_key=True)
    user_id = db.Column(db.Integer(), db.ForeignKey('user.id'))
    role_id = db.Column(db.Integer(), db.ForeignKey('role.id'))

    def get_barcode(self):
        return "*%s*" % (str(self.id).zfill(4))


class Role(db.Model, RoleMixin):
    __tablename__ = 'role'
    created_at = db.Column(db.DateTime(),  default=datetime.datetime.utcnow)
    updated_at = db.Column(db.DateTime(),  onupdate=datetime.datetime.utcnow)

    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))

    def get_barcode(self):
        return "*ROL%s*" % (str(self.id).zfill(4))


class User(db.Model, UserMixin):
    __tablename__ = 'user'
    created_at = db.Column(db.DateTime(),  default=datetime.datetime.utcnow)
    updated_at = db.Column(db.DateTime(),  onupdate=datetime.datetime.utcnow)

    id = db.Column(db.Integer(), primary_key=True)
    active = db.Column(db.Boolean())
    email = db.Column(db.String(255), unique=True)
    password = db.Column(db.String(255))
    confirmed_at = db.Column(db.DateTime())

    login_count = db.Column(db.Integer())
    last_login_at = db.Column(db.DateTime())
    last_login_ip = db.Column(db.String(100))
    current_login_at = db.Column(db.DateTime())
    current_login_ip = db.Column(db.String(100))

    username = db.Column(db.String(255))
    first_name = db.Column(db.String(255))
    middle_name = db.Column(db.String(255))
    last_name = db.Column(db.String(255))
    gender = db.Column(db.String(255), default='female')
    type = db.Column(db.String(255), default='member')
    birthdate = db.Column(db.Date())

    occupation = db.Column(db.String(255))
    company_name = db.Column(db.String(255))
    company_address = db.Column(db.String(255))
    company_position = db.Column(db.String(255))

    emergency_contact = db.Column(db.Text())
    marital_status = db.Column(db.String(255))
    billing_address = db.Column(db.Text())
    home_address = db.Column(db.Text())
    mobile_number = db.Column(db.String(255))
    landline_number = db.Column(db.String(255))

    stored_image_id = db.Column(db.Integer())
    roles = db.relationship('Role', secondary='roles_users',
                            backref=db.backref('users', lazy='dynamic'))

    def get_stored_image(self):
        if not self.stored_image_id:
            return None
        return StoredImage.query.filter_by(id=self.stored_image_id).first()

    def get_barcode(self):
        return "*USR%s*" % (str(self.id).zfill(4))


class GolfCourse(db.Model):
    __tablename__ = 'golf_course'
    created_at = db.Column(db.DateTime(),  default=datetime.datetime.utcnow)
    updated_at = db.Column(db.DateTime(),  onupdate=datetime.datetime.utcnow)

    id = db.Column(db.Integer(), primary_key=True)
    active = db.Column(db.Boolean(), default=True)
    type = db.Column(db.String(25), default='18 holes')
    name = db.Column(db.String(100))
    details = db.Column(db.Text())
    price = db.Column(db.Text(), default=500)
    stored_image_id = db.Column(db.Integer())

    def get_stored_image(self):
        if not self.stored_image_id:
            return None
        return StoredImage.query.filter_by(id=self.stored_image_id).first()

    def get_barcode(self):
        return "*GCR%s*" % (str(self.id).zfill(4))


class Notifications(db.Model):
    __tablename__ = 'notification'
    created_at = db.Column(db.DateTime(),  default=datetime.datetime.utcnow)
    updated_at = db.Column(db.DateTime(),  onupdate=datetime.datetime.utcnow)

    sender_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    receiver_id = db.Column(
        db.Integer, db.ForeignKey('user.id'), nullable=False)
    id = db.Column(db.Integer(), primary_key=True)
    type = db.Column(db.String(25))
    message = db.Column(db.Text())
    data = db.Column(db.Text())
    url = db.Column(db.Text())
    status = db.Column(db.String(25), default='pending')

    def get_barcode(self):
        return "*NTF%s*" % (str(self.id).zfill(4))


class TeeTimeReservation(db.Model):
    __tablename__ = 'tee_time_reservation'
    created_at = db.Column(db.DateTime(),  default=datetime.datetime.utcnow)
    updated_at = db.Column(db.DateTime(),  onupdate=datetime.datetime.utcnow)

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    golf_course_id = db.Column(db.Integer, db.ForeignKey(
        'golf_course.id'), nullable=False)
    golf_course = db.relationship('GolfCourse')

    id = db.Column(db.Integer(), primary_key=True)
    status = db.Column(db.String(25))
    type = db.Column(db.String(25))
    name = db.Column(db.String(100))
    datetime = db.Column(db.DateTime())
    details = db.Column(db.Text())
    status = db.Column(db.String(), default="pending")
    members = db.Column(db.Text())

    tee_time_reservation_members = db.relationship(
        'TeeTimeReservationMember', backref='tee_time_reservation', lazy=True)

    def get_barcode(self):
        return "*TTR%s*" % (str(self.id).zfill(4))

    def get_members(self):
        if self.members:
            members = json.loads(self.members)
        else:
            members = []
        list = []
        list.append(User.query.get(self.user_id))
        for i in members:
            list.append(User.query.get(i))
        return list


class TeeTimeReservationMember(db.Model):
    __tablename__ = 'tee_time_reservation_member'
    created_at = db.Column(db.DateTime(),  default=datetime.datetime.utcnow)
    updated_at = db.Column(db.DateTime(),  onupdate=datetime.datetime.utcnow)

    id = db.Column(db.Integer(), primary_key=True)
    type = db.Column(db.String(25))
    details = db.Column(db.Text())

    tee_time_reservation_id = db.Column(db.Integer, db.ForeignKey(
        'tee_time_reservation.id'), nullable=False)
    __tablename__ = 'tee_time_reservation_member'
    created_at = db.Column(db.DateTime(),  default=datetime.datetime.utcnow)
    updated_at = db.Column(db.DateTime(),  onupdate=datetime.datetime.utcnow)

    id = db.Column(db.Integer(), primary_key=True)
    user_id = db.Column(db.Integer())

    tee_time_reservation_id = db.Column(db.Integer, db.ForeignKey(
        'tee_time_reservation.id'), nullable=False)

    def get_barcode(self):
        return "*TTM%s*" % (str(self.id).zfill(4))


class Facility(db.Model):
    __tablename__ = 'facility'
    created_at = db.Column(db.DateTime(),  default=datetime.datetime.utcnow)
    updated_at = db.Column(db.DateTime(),  onupdate=datetime.datetime.utcnow)

    active = db.Column(db.Boolean(), default=True)
    id = db.Column(db.Integer(), primary_key=True)
    type = db.Column(db.String(25))
    name = db.Column(db.String(100))
    details = db.Column(db.Text())
    status = db.Column(db.String(25))
    capacity = db.Column(db.Integer())
    price = db.Column(db.Float())
    stored_image_id = db.Column(db.Integer())

    def get_stored_image(self):
        if not self.stored_image_id:
            return None
        return StoredImage.query.filter_by(id=self.stored_image_id).first()

    def get_barcode(self):
        return "*FAC%s*" % (str(self.id).zfill(4))


class FacilityReservation(db.Model):
    __tablename__ = 'facility_reservation'
    created_at = db.Column(db.DateTime(),  default=datetime.datetime.utcnow)
    updated_at = db.Column(db.DateTime(),  onupdate=datetime.datetime.utcnow)

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    facility_id = db.Column(db.Integer, db.ForeignKey(
        'facility.id'), nullable=False)
    facility = db.relationship('Facility')

    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(100))
    details = db.Column(db.Text())
    date_start = db.Column(db.Date())
    date_end = db.Column(db.Date())
    status = db.Column(db.String(25), default='pending')

    def get_days(self):
        d0 = self.date_start
        d1 = self.date_end
        delta = d1 - d0
        return delta.days if delta.days > 0 else 1

    def get_barcode(self):
        return "*RSV%s*" % (str(self.id).zfill(4))


class Rental(db.Model):
    __tablename__ = 'rental'
    created_at = db.Column(db.DateTime(),  default=datetime.datetime.utcnow)
    updated_at = db.Column(db.DateTime(),  onupdate=datetime.datetime.utcnow)

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    inventory_item_id = db.Column(db.Integer, db.ForeignKey(
        'inventory_item.id'), nullable=False)
    inventory_item = db.relationship('InventoryItem')

    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(100))
    details = db.Column(db.Text())
    date_start = db.Column(db.Date())
    date_end = db.Column(db.Date())
    status = db.Column(db.String(25), default='pending')

    def get_days(self):
        d0 = self.date_start
        d1 = self.date_end
        delta = d1 - d0
        return delta.days if delta.days > 0 else 1

    def get_barcode(self):
        return "*RSV%s*" % (str(self.id).zfill(4))


class StoredImage(db.Model):
    __tablename__ = 'stored_image'
    created_at = db.Column(db.DateTime(),  default=datetime.datetime.utcnow)
    updated_at = db.Column(db.DateTime(),  onupdate=datetime.datetime.utcnow)

    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(200))
    details = db.Column(db.Text())
    link = db.Column(db.Text())


class InventoryItem(db.Model):
    __tablename__ = 'inventory_item'
    created_at = db.Column(db.DateTime(),  default=datetime.datetime.utcnow)
    updated_at = db.Column(db.DateTime(),  onupdate=datetime.datetime.utcnow)

    active = db.Column(db.Boolean(), default=True)
    id = db.Column(db.Integer(), primary_key=True)
    type = db.Column(db.String(25))
    name = db.Column(db.String(200))
    details = db.Column(db.Text())
    quantity = db.Column(db.Integer())
    price = db.Column(db.Float())
    stored_image_id = db.Column(db.Integer())

    def get_stored_image(self):
        if not self.stored_image_id:
            return None
        return StoredImage.query.filter_by(id=self.stored_image_id).first()

    def get_barcode(self):
        return "*INV%s*" % (str(self.id).zfill(4))


class Invoice(db.Model):
    __tablename__ = 'invoice'
    created_at = db.Column(db.DateTime(),  default=datetime.datetime.utcnow)
    updated_at = db.Column(db.DateTime(),  onupdate=datetime.datetime.utcnow)

    id = db.Column(db.Integer(), primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    details = db.Column(db.Text())

    invoice_items = db.relationship(
        'InvoiceItem', backref='invoice', lazy=True)

    def get_display(self):
        user = User.query.get(self.user_id)
        return "date: %s, id: %s,  user: (%s) %s %s, details: %s" % (self.created_at.date(), self.id, user.email,
                                                                     user.first_name, user.last_name, self.details)

    def get_user(self):
        return User.query.get(self.user_id)

    def get_balance(self):
        items = InvoiceItem.query.filter_by(invoice_id=self.id)
        payments = Payment.query.filter_by(invoice_id=self.id)
        total = sum(item.price for item in items)
        paid = sum(payment.amount for payment in payments)
        left = total - paid
        return left if left > 0 else 0

    def get_total_amount(self):
        items = InvoiceItem.query.filter_by(invoice_id=self.id)
        return sum(item.price for item in items)

    def get_paid_amount(self):
        payments = Payment.query.filter_by(invoice_id=self.id)
        return sum(payment.amount for payment in payments)

    def get_barcode(self):
        return "*RCP%s*" % (str(self.id).zfill(4))


class InvoiceItem(db.Model):
    __tablename__ = 'invoice_item'
    created_at = db.Column(db.DateTime(),  default=datetime.datetime.utcnow)
    updated_at = db.Column(db.DateTime(),  onupdate=datetime.datetime.utcnow)

    id = db.Column(db.Integer(), primary_key=True)
    type = db.Column(db.String(25))
    name = db.Column(db.String(200))
    details = db.Column(db.Text())
    price = db.Column(db.Float())

    inventory_item_id = db.Column(db.Integer(), nullable=True)
    invoice_id = db.Column(db.Integer, db.ForeignKey(
        'invoice.id'), nullable=False)

    def get_inventory_item(self):
        return InventoryItem.query.get(id=self.inventory_item_id)

    def get_barcode(self):
        return "*RCI%s*" % (str(self.id).zfill(4))


class Payment(db.Model):
    __tablename__ = 'payment'
    created_at = db.Column(db.DateTime(),  default=datetime.datetime.utcnow)
    updated_at = db.Column(db.DateTime(),  onupdate=datetime.datetime.utcnow)

    id = db.Column(db.Integer(), primary_key=True)
    type = db.Column(db.String(25), default='cash')
    amount = db.Column(db.Float(), default=0.0)
    details = db.Column(db.Text(), default='')

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    invoice_id = db.Column(db.Integer, db.ForeignKey(
        'invoice.id'), nullable=False)

    def get_invoice(self):
        return Invoice.query.filter_by(id=self.invoice).first()

    def get_user(self):
        return User.query.filter_by(id=self.user_id).first()

    def get_get_barcode(self):
        return "*PAY%s*" % (str(self.id).zfill(4))


# rentals
# - return  by the end of the day
# - add penalty per day late

# resertvation
# - maintenance must reservew facilty to block it off

# class Rentals(db.Model):
#     __tablename__ = 'rental'
#     created_at = db.Column(db.DateTime(),  default=datetime.datetime.utcnow)
#     updated_at = db.Column(db.DateTime(),  onupdate=datetime.datetime.utcnow)

#     id = db.Column(db.Integer(), primary_key=True)
#     type = db.Column(db.String(25))
#     details = db.Column(db.Text())
#     amount = db.Column(db.Float())

#     user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
#     invoice_id = db.Column(db.Integer, db.ForeignKey(
#         'invoice.id'), nullable=False)
