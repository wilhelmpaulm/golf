# Golf MIS

## description
- this is intended to be a a generic golf club management information system

----

## installation of system environment requirements (simple)
- install [`python 3`](https://www.python.org/ftp/python/3.6.4/python-3.6.4.exe)
- we want to make sure that [`python`](https://www.python.org/ftp/python/3.6.4/python-3.6.4.exe) is installed
- - open `cmd`
- - execute `> python --version`
- - you should get something like `Python 3.6.3`
- - - ![install](docs/installation/01.PNG)
- make sure [`pip`](https://packaging.python.org/tutorials/installing-packages/) is installed (installed with python)
- - `pip` or `Pip Installs Packages`
- - is the `python` package manager, this enables you to install packages created by other developers
- while you're in `cmd` and the location is `inside the golf (project) folder`
- example: `C:\Users\<user_name>\Desktop\golf`
- - ![install](docs/installation/02.PNG)
- - ![install](docs/installation/03.PNG)
- execute `> pip install -r requirements.txt`
- - ![install project requirements](docs/installation/11.PNG)
- - this installs the dependencies of the project

----

## installation of system environment requirements (with virtualenv)
- install [`python 3`](https://www.python.org/ftp/python/3.6.4/python-3.6.4.exe)
- we want to make sure that [`python`](https://www.python.org/ftp/python/3.6.4/python-3.6.4.exe) is installed
- - open `cmd`
- - execute `> python --version`
- - you should get something like `Python 3.6.3`
- - - ![install](docs/installation/01.PNG)
- make sure [`pip`](https://packaging.python.org/tutorials/installing-packages/) is installed (installed with python)
- - `pip` or `Pip Installs Packages`
- - is the `python` package manager, this enables you to install packages created by other developers
- install the `python` package [`virtualenv`](https://virtualenv.pypa.io/en/stable/userguide/)
- - `virtualenv` creates a stand alone python installation in your preferred folder
- - you can install it by executing 
- - `> pip install virtualenv`
- - - ![install](docs/installation/011.PNG)

----

## installation of project requirements
- once `virtualenv` is installed in your system
- we will want to create a `stand alone python installation` which will make sure that we don't damage the current installations in the computer
- we want to create this `stand alone python installation` by using `virtualenv` inside our project folder
- - while you're in `cmd` and the location is `inside the golf folder`
- - example: `C:\Users\<user_name>\Desktop\golf`
- - - ![install](docs/installation/02.PNG)
- - - ![install](docs/installation/03.PNG)
- - execute `> virtualenv golf_env`
- - - `golf_env` is the folder name of our `stand alone python installation`
- - - - ![install](docs/installation/04.PNG)
- once that is finished, we then want to install the required `packages` of the project
- - first we need to tell windows to run python from our `golf_env` folder
- - we do this by executing `> golf_env\Scripts\activate.bat`
- - - ![install](docs/installation/05.PNG)
- - - ![check where pip executed](docs/installation/06.PNG)
- - - - just to check where pip is now being run
- - execute `> pip install -r requirements.txt`
- - - ![install project requirements](docs/installation/07.PNG)
- - - this installs the dependencies of the project

----

## activating the `virtualenv` in `\project folder\golf_env`
- while inside the project folder
- execute `> golf_env\Scripts\activate.bat`
- - ![install](docs/installation/05.PNG)
- you'll know that `golf_env` is activated because the command line is prefixed by `(golf_env)` 

----

## running the project server
- if all the requirements are installed
- if on simple setup:
- execute `>python base.py` to start the server
- - ![start server](docs/installation/10.PNG)
- if using virtual env 
- - while `golf_env` is activated
- - execute `>python base.py` to start the server
- - ![start server](docs/installation/08.PNG)


----

## clearing the database and creating the default users
- start the server
- - execute `>python base.py` to start the server
- - go to `127.0.0.1:5000/migrate_db`
- - ![migrate db](docs/installation/12.PNG)
- this deletes the database and creates a new one
- this also creates default users
- - (email='user@example.com', password='password', roles=['user'])
- - (email='admin@example.com', password='password', roles=['admin'])

----

## components
- club member information management
- golf course information management
- - events
- - maintenance
- tee time reservation

----

## libraries we'll be using
- for theming
- - [bootstrapious admin theme](https://demo.bootstrapious.com/admin-premium/1-3-2/forms-advanced.html)
- for the pretty dropdown
- - [bootstrap-select](https://silviomoreto.github.io/bootstrap-select/examples/)
- for managing dates in JS
- - [moment JS](https://momentjs.com)
- for date, date ranges
- - [tempusdominus](https://tempusdominus.github.io/bootstrap-4/Usage/)
- for datetime and time
- - [bootstrap-datetimepicker](https://eonasdan.github.io/bootstrap-datetimepicker/)
- table sorting and search
- - [data tables](https://datatables.net/examples/styling/bootstrap4.html)
- simple sorting
- - [list JS](http://listjs.com/)
- for the weather
- - [yahoo weather API](https://developer.yahoo.com/weather/#python)


## guide to creating new forms
- I have trouble doing the frontend due to limited time
- for most parts, they don't require anything complex
- we could start using tools such as:
- - https://bootstrapformbuilder.com/

----

## TODO:
- items to be accomplished
