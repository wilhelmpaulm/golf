import os

class BaseConfig:
    BASEDIR = os.path.abspath(os.path.dirname(__file__))
    PROJECT_NAME = "starter"

    DEBUG = True
    SECRET_KEY = "$2a$12$LdKsgm9HGNC6LzKVzJ48ju"

    SQLALCHEMY_DATABASE_URI = "sqlite:///database/golf.db"

    # Debug
    ASSETS_MINIFY = False
    ASSETS_USE_CDN = False

    MAIL_SERVER = 'smtp.gmail.com'
    MAIL_PORT = 465
    MAIL_USE_SSL = True
    MAIL_USERNAME = 'dev.wilhelmpaulm@gmail.com'
    MAIL_PASSWORD = 'dev.password'

    DEFAULT_MAIL_SENDER = ("dev.wilhelmpaulm", "dev.wilhelmpaulm@gmail.com")

    # Flask-Security Flags
    SECURITY_SEND_REGISTER_EMAIL = False
    SECURITY_CONFIRMABLE = False
    SECURITY_REGISTERABLE = True
    SECURITY_RETYPABLE = True
    SECURITY_RECOVERABLE = True
    SECURITY_TRACKABLE = True

    SECURITY_CONFIRM_ERROR_VIEW = "/after-login"
    SECURITY_POST_LOGIN_VIEW = "/after-login"
    SECURITY_POST_REGISTER_VIEW = "/after-login"
    SECURITY_POST_LOGOUT_VIEW = "/after-login"

    SECURITY_PASSWORD_HASH = "bcrypt"

    SECURITY_PASSWORD_SALT = "$2a$12$sSoMBQ9V4hxNba5E0Xl3Fe"
    SECURITY_CONFIRM_SALT = "$2a$12$QyCM19UPUNLMq8n225V7qu"
    SECURITY_RESET_SALT = "$2a$12$GrrU0tYteKw45b5VfON5p."
    SECURITY_REMEMBER_SALT = "$2a$12$unlKF.sL4gnm4icbk0tvVe"

    SQLALCHEMY_TRACK_MODIFICATIONS = True


class ProductionConfig(BaseConfig):
    DEBUG = False
    PROPAGATE_EXCEPTIONS = True

    ASSETS_MINIFY = True
    ASSETS_USE_CDN = True
