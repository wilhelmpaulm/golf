$(document).ready(function () {
    // $('select').prepend("<option value='' selected='selected'></option>");
    $('select').chosen({
        max_selected_options: 3,
        allow_single_deselect: true,
        search_contains: true,
        placeholder_text_single: "Please select an option in the list.",
        placeholder_text_multiple: "Select up to 3 members.",
        no_results_text: "Option is not available. Please contact support at vthorium@gmail.com for help with your query: "
    });
    // $('.datatable').DataTable();
    // $('table').addClass();
    $('table').DataTable();

    // $('.btn').addClass('btn-sm');
    // $('.pagination').addClass('btn-sm');

    $('body').on('click', '.del', function () {
        if (!confirm("Are you sure you want to delete this item?")) {
            return false;
        }
    });

    $('.selectpicker').selectpicker({
        // style: 'btn-info',
        // size: 4
    });

});